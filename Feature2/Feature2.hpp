//
// Created by Sankaranarayanan G on 2019-01-29.
//

#ifndef CICDTESTING_FEATURE2_HPP
#define CICDTESTING_FEATURE2_HPP


class Feature2 {
public:
    long long int Fibonacci (int n);

    int GCD (int a, int b);
};


#endif //CICDTESTING_FEATURE2_HPP
