import gitlab
import os
import re


token = 'GYV2U8-xi_9qHCnHCop5'
url = 'https://gitlab.com'

os.system ("git clone https://gitlab.com/gkp1998/badgefolder.git")
os.chdir("badgefolder")

job_names = ["LINUX_x86_64" , "macOS_x86_64", "WINDOWS_x86_64"]


# Generate the "running" badges for all the platforms
os.system ("git config --global user.email 'gkp1998@gmail.com'")
os.system ("git config --global user.name 'gkp1998'")

for j in job_names:
    os.system("cp "+j+"-running.svg "+j+".svg")
    os.system ("git add "+j+".svg")
os.system("ls")
os.system ("git commit -m '[skip ci] commit'")
os.system ("git push origin master")

os.chdir("..")
os.system("pwd")

# Get all the job URLS
gl = gitlab.Gitlab(url, token, api_version=4)
project = gl.projects.get(os.environ['CI_PROJECT_ID'], lazy=True)
last_pipeline = project.pipelines.get (os.environ['CI_PIPELINE_ID'])
jobs = last_pipeline.jobs.list()

job_names_url_map = {}

for j in jobs:
    if j.name in job_names:
        job_names_url_map[j.name] = j.web_url


# Update the README.md file with the obtained URLs
with open('README.md', 'r') as file:
    data = file.readlines()

for index in range(0, len(data)):
    line = data[index]
    token = re.findall(r'\([^()]*\)',line )
    for i in job_names:
        if i in line:
            line = line.replace (token[1], "(" + job_names_url_map[i] + ")")
            data[index] = line
            break

with open('README.md', 'w') as file:
    file.writelines( data )

