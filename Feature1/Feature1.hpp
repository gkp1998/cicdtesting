//
// Created by Sankaranarayanan G on 2019-01-29.
//

#ifndef CICDTESTING_FEATURE1_HPP
#define CICDTESTING_FEATURE1_HPP

class Feature1 {
public:
    long long int Fibonacci (int n);

    int GCD (int a, int b);
};

#endif //CICDTESTING_FEATURE1_HPP
