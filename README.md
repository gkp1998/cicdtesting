| __PLATFORM__     | __STATUS__ |
|:-------:|:-------:|
|__WINDOWS_x86_64__|[![badge](https://gitlab.com/gkp1998/badgefolder/raw/master/WINDOWS_x86_64.svg)](https://gitlab.com/gkp1998/cicdtesting/-/jobs/260030386)|
|__LINUX_x86_64__|[![badge](https://gitlab.com/gkp1998/badgefolder/raw/master/LINUX_x86_64.svg)](https://gitlab.com/gkp1998/cicdtesting/-/jobs/260030378)|
|__macOS_x86_64__|[![badge](https://gitlab.com/gkp1998/badgefolder/raw/master/macOS_x86_64.svg)](https://gitlab.com/gkp1998/cicdtesting/-/jobs/260030382)|