//
// Created by Sankaranarayanan G on 2019-01-29.
//

#include "Feature1.hpp"
#include <algorithm>

using namespace std;

long long int Feature1::Fibonacci (int n) {
    long long arr[12];
    arr[1] = 0;
    arr[2] = 1;

    for (int i = 3; i <= n; i++) {
        arr[i] = arr[i - 1] + arr[i - 2];
    }
    return arr[n];
}

int Feature1::GCD (int a, int b) {

    while (b != 0) {
        int temp = b;
        b = a % b;
        a = temp;
    }
    return a;
}
