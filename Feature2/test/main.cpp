//
// Created by Sankaranarayanan G on 2019-01-29.
//

#include <gtest/gtest.h>
int main (int argc, char** argv) {
    ::testing::GTEST_FLAG(output) = "xml:feature2report.xml";
    ::testing::InitGoogleTest (&argc, argv);
    return RUN_ALL_TESTS ();
}
